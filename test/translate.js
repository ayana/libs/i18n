'use strict';

const { I18N } = require('../src');
const i18n = new I18N('en-US');

i18n.addTranslations('en-US', {
	FUCKS_GIVEN: 'You give {numFucks, plural, =0 {no fucks} =1 {exacly one whole fuck} other {a shit ton of fucks (#)}}',
	STR_JUAN: '',
	STR_TOO: '',
	EMPTY_STRING: 'en-US empty string',
});

i18n.addTranslations('ja-JA', {
	FUCKS_GIVEN: 'ウエブルーン, {numFucks, number, #}',
})

i18n.addTranslations('en-UK', {
	EMPTY_STRING: '',
});

console.log(i18n.hasTranslation('en-US', 'FUCKS_GIVEN'));
console.log(i18n.hasTranslation('en-US', 'FUCKS_GIVEN_NOT'));

console.log(`ja-JA is ${i18n.getPercentTranslated('ja-JA')}% translated!`);

console.log(i18n.translate('en-US', 'FUCKS_GIVEN', { numFucks: 0 }));
console.log(i18n.translate('en-US', 'FUCKS_GIVEN', { numFucks: 1 }));
console.log(i18n.translate('en-US', 'FUCKS_GIVEN', { numFucks: 100000 }));


console.log(i18n.translate('ja-JA', 'FUCKS_GIVEN', { numFucks: 1000000 }));

console.log(i18n.translate('en-UK', 'EMPTY_STRING'));

console.log(i18n.translate('im.not.real', 'BLAH'));
